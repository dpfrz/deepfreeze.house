#!/bin/bash

# Note: Make sure to run the below commented-out line
# before this script, if you make changes to the Docker
# image:
# ./push-docker.sh

gcloud run deploy deepfreeze-$(basename `pwd` | cut -d. -f2) \
  --concurrency 8 \
  --cpu 1 \
  --memory 256Mi \
  --min-instances 0 \
  --max-instances 8 \
  --platform managed \
  --service-account deepfreeze-$(basename `pwd` | cut -d. -f2)@deepfreeze.iam.gserviceaccount.com \
  --timeout 300s \
  --no-use-http2 \
  --allow-unauthenticated \
  --region us-west1 \
  --image gcr.io/deepfreeze/deepfreeze-$(basename `pwd` | cut -d. -f2)
