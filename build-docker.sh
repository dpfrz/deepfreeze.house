#!/bin/bash

docker build -t gcr.io/deepfreeze/deepfreeze-$(basename `pwd` | cut -d. -f2) .
