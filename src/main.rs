#![feature(proc_macro_hygiene, decl_macro)]
#![feature(plugin)]

const OWNER: &'static str = include_str!("dpfrz-owner.conf");
const OWNID: &'static str = include_str!("dpfrz-owner.conf");

extern crate rocket;
extern crate rocket_contrib;

use rocket::*;
use rocket::Request;
use rocket_contrib::templates::Template;
use rocket::response::NamedFile;
use std::path::Path;
use std::path::PathBuf;
use std::io;

use std::collections::HashMap;
use std::env;

fn context<'a>() -> HashMap<&'a str, u64> {
    HashMap::from([
        // ("a", 1),
    ])
}

#[get("/hi/<nick>/<numb>")]
fn hi(nick: String, numb: String) -> Template {
    let ctx = context();

    if nick == OWNER.lines().next().unwrap() && numb == OWNID.lines().nth_back(0).unwrap() {
        println!("OWNER=\"{}\"", OWNER);
        println!("hi: {} {}", nick, numb);
        return Template::render("hi", &ctx);
    } else {
        println!("error: user page not found: {} {}", nick, numb);
        return Template::render("404", &ctx);
    }
}

#[get("/<file..>", rank=2)]
fn files(file: PathBuf) -> io::Result<NamedFile> {
    let page_directory_path = 
    "frontend/build";

    let templates_directory_path = "templates";

    match NamedFile::open(Path::new(&page_directory_path).join(file)) {
        Ok(file) => {
            Ok(file)
        },

        Err(e) => {
            println!("error: {}", e);
            NamedFile::open(Path::new(&templates_directory_path).join("404.html"))
        }
    }
}

#[get("/")]
fn frontend() -> io::Result<NamedFile> {
  let page_directory_path = 
  "frontend/build";
  NamedFile::open(Path::new(&page_directory_path).join("index.html"))
}

#[catch(404)]
fn not_found(req: &Request) -> String {
    format!("Sorry, '{}' is not a valid path.", req.uri())
}

fn main() {
    let mut port: u16 = 8080;
    match env::var("PORT") {
        Ok(p) => {
            match p.parse::<u16>() {
                Ok(n) => {
                    port = n;
                }
                Err(_e) => {}
            };
        }
        Err(_e) => {}
    };

    env::set_var("ROCKET_PORT", &format!("{}", port));

    let rock = rocket::ignite()
    .register(catchers![not_found])
    .attach(Template::fairing());

    let routes = routes![
        hi, 
        files, 
        frontend,
    ];

    rock
    .mount("/", routes).launch();
}
