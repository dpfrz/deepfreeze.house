#!/bin/sh

docker stop deepfreeze-$(basename `pwd` | cut -d. -f2)
docker run --rm -itd -p 8080:8080 --name deepfreeze-$(basename `pwd` | cut -d. -f2) gcr.io/deepfreeze/deepfreeze-$(basename `pwd` | cut -d. -f2)
docker logs deepfreeze-$(basename `pwd` | cut -d. -f2) -f
