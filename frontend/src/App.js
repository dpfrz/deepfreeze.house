import logo from './logo.svg';
import './App.css';
import './themify-icons.css';

function App() {
  return (
    <div className="App">
      <header className="App-header">
        Deepfreeze
      </header>

      <p/>

      <div>
        <a href="/hi/defcron/23453457895926579836543985679423509">
          <span class="icon ti-user"></span>
        </a>

        <span> &nbsp; &nbsp;</span>

        <a href="https://com.deepfreeze.me">
          <span class="icon ti-headphone-alt"></span>
        </a>
      </div>
    </div>
  );
}

export default App;
