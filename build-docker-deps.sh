#!/bin/bash

docker build -f Dockerfile-deps -t gcr.io/deepfreeze/deepfreeze-$(basename `pwd` | cut -d. -f2)-deps .
