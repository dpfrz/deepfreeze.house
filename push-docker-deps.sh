#!/bin/bash

# NOTE: You should consider not deploying this deps image unless you
# need it online somewhere for CI/CD purpose.

docker push gcr.io/deepfreeze/deepfreeze-$(echo $PWD | awk -F/ '{print $NF}' | awk -F. '{print $NF}')-deps
