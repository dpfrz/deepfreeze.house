# -- Build Image --
FROM gcr.io/deepfreeze/deepfreeze-house-deps:latest as build

WORKDIR /usr/src/app

COPY Cargo.lock .
COPY Cargo.toml .
COPY ./src src

RUN cargo build --release


# -- Production Image --
FROM scratch

COPY ./frontend/build/ frontend/build

COPY ./templates templates

COPY --from=build /usr/src/app/target/x86_64-unknown-linux-musl/release/deepfreeze-house .

CMD [ "./deepfreeze-house" ]
