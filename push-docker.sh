#!/bin/bash

docker push gcr.io/deepfreeze/deepfreeze-$(echo $PWD | awk -F/ '{print $NF}' | awk -F. '{print $NF}')
